
import a1planets.planet as planet
def test_planet_constructor():
    'Test that a planet object is built as expected'

    name='Pluto' 
    mass=42.0
    x=6.0
    y=6.0
    vx=13.0
    vy=13.0
    color='r'
    plan=planet.Planet(name,mass,x,y,vx,vy,color)
    assert plan.name == name
    assert plan.mass == mass
    assert plan.pos.x == x
    assert plan.pos.y == y
    assert plan.vel.x == vx
    assert plan.vel.y == vy
    assert plan.color == color
