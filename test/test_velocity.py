import a1planets.velocity as velocity
    
    
def test_velocity_constructor():
    '''
    Test that a velocity object is built as expected
    '''
    vx=6.0
    vy=6.0
    
    vel=velocity.Velocity(vx,vy)
    
    assert vel.x==vx
    assert vel.y==vy
