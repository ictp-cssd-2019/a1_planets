
import a1planets.position as position

def test_position_constructor():
    'Test that an Position object is constructed as expected'
    pos=position.Position(1.,2.)

    assert pos.x==1.
    assert pos.y==2.
	
