class Velocity:
    """
    This class is a velocity object to be used inside a planet.
    """    
    def __init__(self,vx,vy):
        """
        Contructor function
        :type vx: float
        :param vx: : velocity in x
        :type vy: float
        :param vy: : velocity in y
        """
        self.x=vx
        self.y=vy
