from . import position
from . import velocity

class Planet:
    """
    This class is a planet object
    The planet object includes a position and velocity objects
    """
    def __init__(self,name,mass,x_0,y_0,vx_0,vy_0,color):
        """
        Constructor function

        :type name: string
                :param name: : name of the planet
        :type mass: float
                :param mass: : mass of the planet
        :type x_0: float
                :param x_0: : initial position in x axis
        :type y_0: float
                :param y_0: : initial position in y axis
        :type vx_0: float
                :param vx_0: : initial velocity in x axis
        :type vy_0: float
                :param vy_0: : initial velocity in y axis
                :type color: string
                :param color: :color of the planet. Matplotlib color abreviation
        """
        self.name=name
        self.mass=mass
        self.pos=position.Position(x_0,y_0)
        self.vel=velocity.Velocity(vx_0,vy_0)
        self.pos_new=position.Position(0.,0.)
        self.vel_new=velocity.Velocity(0.,0.)
        self.color=color
