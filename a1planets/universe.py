from . import position
class Universe:
    '''
    This class contains the celestial_objects
    Calculate the center of mass
    Calculate the trajectory of a list of planets
    '''
    def __init__(self,G,time_step,runtime,planets):
        self.G=G
        self.center_mass=position.Position(0,0)
        self.planets=planets
        self.time_step=time_step
        self.runtime=runtime
        self.set_center_of_mass(self.planets)
    
    def cal_center_of_mass(self,planets):
        """
        This function recieves planets and calculates their center of mass
        :type planets: object
        :param palnets: gets the list of planets from universe
        :returns: two floats (CM_x, CM_y)
        """
        CM_x = 0.
        CM_y = 0.
        tot_mass = 0.
        for planet in planets:
            CM_x += planet.pos.x*planet.mass
            CM_y += planet.pos.y*planet.mass
            tot_mass += planet.mass
        CM_x /= tot_mass
        CM_y /= tot_mass
        return CM_x, CM_y
        
    def set_center_of_mass(self, planets):
        """
        This function sets the planets positions according to the unvivers center of mass
        :type planets: object
        :param palnets: gets the list of planets from universe
        """
        CM_x, CM_y = self.cal_center_of_mass(planets)
        for planet in planets:
            planet.pos.x -= CM_x
            planet.pos.y -= CM_y
    
    def plot_trajectory(self,celestial_object_list):
        '''
        Input: list of objects of type celestialObject
        Read the trajectory array of each celestial_object
        Plot the trajectory in a pyplot canvas
        '''
        
        pass
    
    
