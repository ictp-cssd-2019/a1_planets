"""
This module is the driver of the planetary motion simulation

In this module we calculate the forces acting on each body and
we use RK4 method to update positions and velocities of palnets.
"""
import math
def cal_acc(x,y,name,universe):
    """
    This function updates the acceleration for each planet using
    the Newton law for gravitation 
        
    :type x: float
    :param x: : x pos to the Current Planet
    :type y: float
    :param y: : y pos to the Current Planet
    :type name: string
    :param name: : name of the planet
    :type universe: object
    :param universe: : The universe contating all the planets
    
    :returns: accelaration at each step in x and y directions(ax, ay) 
    """

    ax = 0
    ay = 0
    for planet in universe.planets:
        if planet.name.strip() != name.strip():
            dx = x - planet.pos.x
            dy = y - planet.pos.y
            r = math.sqrt(dx**2+dy**2)
            if (r<0.001): 
                msg='planets '+planet.name+' and '+name+' collapsed.'
                exit(msg)
            ax -= (universe.G * planet.mass * dx)/(r**3)
            ay -= (universe.G * planet.mass * dy)/(r**3)
    return ax, ay

def RK4(CP,universe):
    """
    This is the Runge_Kutta calculation function.
        
    :type CP: object
    :param CP: The object of the current planet for which new posintions and velocities will be computed but not updated
    :type universe: object
    :param universe: The object of the universe which contains all the planets
    """
    
    h = float(universe.time_step)
    
    k1x = CP.vel.x
    k1y = CP.vel.y

    g1x,g1y = cal_acc(CP.pos.x,CP.pos.y,CP.name,universe)
    
    k2x = CP.vel.x+0.5*h*g1x
    k2y = CP.vel.y+0.5*h*g1y

    g2x,g2y = cal_acc(CP.pos.x+0.5*h*k1x,CP.pos.y+0.5*h*k1y,CP.name,universe)

    k3x = CP.vel.x+0.5*h*g2x
    k3y = CP.vel.y+0.5*h*g2y
    g3x,g3y = cal_acc(CP.pos.x+0.5*h*k2x,CP.pos.y+0.5*h*k2y,CP.name,universe)
    
    k4x = CP.vel.x+h*g3x
    k4y = CP.vel.y+h*g3y

    g4x,g4y= cal_acc(CP.pos.x+h*k3x,CP.pos.y+h*k3y,CP.name,universe)
    
    CP.pos_new.x = CP.pos.x + (h/float(6.))*(k1x+2.0*k2x+2.0*k3x+k4x)
    CP.pos_new.y = CP.pos.y + (h/float(6.))*(k1y+2.0*k2y+2.0*k3y+k4y)
    CP.vel_new.x = CP.vel.x + (h/float(6.))*(g1x+2.0*g2x+2.0*g3x+g4x)
    CP.vel_new.y = CP.vel.y + (h/float(6.))*(g1y+2.0*g2y+2.0*g3y+g4y)
def set_pos(universe):
    """
    This function updates planets positions and velocites once the new position and velocity for all of them is calculated. 
        
    :type universe: object
    :param universe: The object of the universe which contains all the planets
    """
    for planet in universe.planets:
        planet.pos.x=planet.pos_new.x
        planet.pos.y=planet.pos_new.y
        planet.vel.x=planet.vel_new.x
        planet.vel.y=planet.vel_new.y
