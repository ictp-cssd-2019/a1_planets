from . import planet
from . import universe
import configparser
import sys

def read_file(file_path):
    '''
    This function check if the file exist as configparser 
    does not check it.
    '''
    try:
        with open(file_path) as parameter_file:
            return file_path        

    except Exception as e:
        print("Error in read_file(%s). Error: %s " %(file_path,str(e))) 
        sys.exit(-1)

def read_parameters(file_path):
    """
    This function will read an input file with following parameters:
    Name of planet as 'name'
    Color of PLanet as 'color'
    Mass of Planet as 'mass'
    Position of x as 'xi' 
    Position of y as 'yi'
    Velocity of x as 'vx'
    Velocity of y as 'vy'
    """
    parameter_file=read_file(file_path)
    parser=configparser.ConfigParser()
    parser.read(parameter_file)
    planets_list=[] 
    list_objects=parser.sections()

    for section in list_objects:
        object_properties=parser.items(section)
        if section == list_objects[0]:
            G = float(object_properties[0][1])
            runtime = float(object_properties[1][1])
            timestep = float(object_properties[2][1])
        else:
            name=object_properties[0][1]
            mass=float(object_properties[2][1])
            x_0 =float(object_properties[3][1])
            y_0 =float(object_properties[4][1])
            vx_0 =float(object_properties[5][1])
            vy_0 =float(object_properties[6][1])
            color=object_properties[1][1]
            tmp_planet=planet.Planet(name,mass,x_0,y_0,vx_0,vy_0,color)
            planets_list.append(tmp_planet)

    return universe.Universe(G,timestep,runtime,planets_list)
