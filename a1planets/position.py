class Position:
    """
    This class is a position object used by planet and universe objects
    """
    def __init__(self,x,y):
        """
        :type x: float
        :param x: : coordinate in x
        :type y: float
        :param y: : coordinate in y
        """
        self.x=x
        self.y=y


