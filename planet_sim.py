#!/usr/bin/env python3
#import universe
#import driver
#import inputparameter
from a1planets import inputparameter as IP
from a1planets import driver as DR
import matplotlib.pyplot as plt
import numpy as np
import sys
import math


def main(universe_object):
    '''
    Call the driver with an object universe as input calculates the position in realtime and
    updates the position    
    '''
    for planet in universe_object.planets:
        DR.RK4(planet,universe_object)
    DR.set_pos(universe_object)

def set_planet_dict(universe_object):
    '''
    Creates a planet dictionary with a line and circle objects
    '''
    planet_d={}
    uni_max_x=max([math.fabs(u.pos.x) for u in universe_object.planets[:]])
    for planet in universe_object.planets:
		
        line_object=axes.plot([],[],lw=2,color=planet.color)[0]
        line_object.set_data([],[])
        circle_object=plt.Circle((0,0),radius=0.03*uni_max_x,color=planet.color)
        axes.add_patch(circle_object)	
        planet_d[planet.name]=(line_object,circle_object,np.zeros((int(runtime),2),dtype=np.float64))

    return planet_d

def plot_planet_position(i,planet_dict):
    '''
    Fill the trajectory array of each planet with its actual position
    Center the ball according to the planets position in that moment
    Plot the accumulated trajectory line 
    '''
    for planet in universe_object.planets:
		
        planet_dict[planet.name][2][i,0]=planet.pos.x
        planet_dict[planet.name][2][i,1]=planet.pos.y
        planet_dict[planet.name][1].center=(planet.pos.x,planet.pos.y)
        planet_dict[planet.name][0].set_data(planet_dict[planet.name][2][:i,0],planet_dict[planet.name][2][:i,1])

try:
    inputparameters_file=sys.argv[1]
except Exception as e:
    print("Please specify an input file: %s)" %str(e))
universe_object = IP.read_parameters(inputparameters_file)
figure=plt.figure()
uni_max_x=max([math.fabs(u.pos.x) for u in universe_object.planets[:]])
uni_max_x *= 1.3
axes=plt.axes(xlim=(-1*uni_max_x,uni_max_x),ylim=(-1*uni_max_x,uni_max_x))
runtime=int(universe_object.runtime)
planet_dict={}
planet_dict=set_planet_dict(universe_object)


for i in range(runtime):
    '''
    Call main function to calculate the position for each planet
    Then call plot_planet_position to update this data.
    Call plt.pause for a 'crude animation'
    '''
    #print(i)
    main(universe_object)
    plot_planet_position(i,planet_dict)
 #   if (math.fmod(i,int(universe_object.time_step*universe_object.runtime/300))==0):
    if (math.fmod(i,math.floor(5.e-2/universe_object.time_step))==0):
        plt.title(str(i))
        plt.pause(1.e-8)
