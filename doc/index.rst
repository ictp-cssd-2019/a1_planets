.. a1planet documentation master file, created by
   sphinx-quickstart on Wed May  8 12:22:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to a1planet's documentation!
=====================================


.. include:: ./.txt/Introduction.txt
.. include:: ./.txt/Methods.txt

.. automodule:: a1planets
    :members:

.. include:: ./.txt/inputparameters.txt

.. include:: ./.txt/Classes.txt

.. automodule:: a1planets.position
    :members:

.. automodule:: a1planets.velocity
    :members:

.. automodule:: a1planets.planet
    :members:

.. automodule:: a1planets.universe
    :members:

.. automodule:: a1planets.driver
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:
